# Stygian

## About
Stygian is a data recorder for USB and TCP devices communicating via the Asphodel protocol.

The Asphodel communication protocol was developed by Suprock Technologies (http://www.suprocktech.com)

## License
Stygian is licensed under the ISC license.

The ISC license is a streamlined version of the BSD license, and permits usage in both open source and propretary projects.
